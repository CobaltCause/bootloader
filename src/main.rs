#![no_main]
#![no_std]
#![feature(abi_efiapi)]
#![allow(stable_features)]

use anyhow::anyhow;
use log::{debug, error, info};

extern crate alloc;
use alloc::boxed::Box;
use slint::{platform::software_renderer::MinimalSoftwareWindow, ComponentHandle, VecModel};
use uefi::{
    prelude::entry,
    table::{Boot, SystemTable},
    Handle, Status,
};

use crate::platform::UefiPlatform;

#[allow(clippy::eq_op)]
mod ui {
    slint::include_modules!();
}
mod platform;

fn real_main(system_table: &mut SystemTable<Boot>) -> anyhow::Result<()> {
    info!("Setting up UEFI services...");

    uefi_services::init(system_table)
        .map_err(|e| anyhow!("Error setting up UEFI services: {:?}", e))?;

    info!("Setting up platform...");

    let window = MinimalSoftwareWindow::new();
    let platform = Box::new(UefiPlatform::new(window.clone(), system_table)?);
    slint::platform::set_platform(platform).unwrap(); // panics iff platform is already set

    info!("Setting up UI...");

    let ui = ui::BootMenu::new();
    ui.set_entries(
        VecModel::from_slice(&[
            ui::BootEntry {
                name: "This".into(),
            },
            ui::BootEntry {
                name: "That".into(),
            },
        ])
        .into(),
    );
    ui.on_selected(|evt| {
        debug!("Selected: {:?}", evt);

        // panics iff no event loop is running
        slint::quit_event_loop().unwrap();

        info!("Shut down slint!");
    });

    ui.run();

    Ok(())
}

#[entry]
fn main(_handle: Handle, mut system_table: SystemTable<Boot>) -> Status {
    match real_main(&mut system_table) {
        Ok(()) => Status::SUCCESS,
        Err(e) => {
            error!("Error: {:?}", e);
            system_table.boot_services().stall(10_000_000);
            Status::ABORTED
        }
    }
}
