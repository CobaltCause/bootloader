use chrono::{FixedOffset, TimeZone, Utc};
use uefi::prelude::RuntimeServices;

pub(crate) struct TimeWrapper<'a> {
    runtime_services: &'a RuntimeServices,
}

impl<'a> TimeWrapper<'a> {
    // FIXME: this probably needs to use a higher precision timer somehow
    pub(crate) fn new(runtime_services: &'a RuntimeServices) -> Self {
        Self { runtime_services }
    }

    pub(crate) fn get_time(&self) -> chrono::DateTime<Utc> {
        let efi_time = self
            .runtime_services
            .get_time()
            .expect("Failed to get time!");

        let offset = match efi_time.time_zone() {
            None => 0,
            Some(offset) => offset as i32 * 60,
        };

        FixedOffset::east_opt(offset)
            .unwrap()
            .with_ymd_and_hms(
                efi_time.year() as i32,
                efi_time.month() as u32,
                efi_time.day() as u32,
                efi_time.hour() as u32,
                efi_time.minute() as u32,
                efi_time.second() as u32,
            )
            .unwrap()
            .with_timezone(&Utc)
    }
}
