use anyhow::anyhow;
use uefi::{proto::console::pointer::Pointer, table::boot::ScopedProtocol};

pub(crate) struct MouseWrapper<'a> {
    protocol: ScopedProtocol<'a, Pointer<'a>>,
    x: f32,
    y: f32,
    left: bool,
    right: bool,
}

#[derive(Debug)]
pub(crate) struct MouseEvent {
    pub(crate) x: f32,
    pub(crate) y: f32,
    pub(crate) left_down: bool,
    pub(crate) left_up: bool,
    pub(crate) right_down: bool,
    pub(crate) right_up: bool,
}

impl<'a> MouseWrapper<'a> {
    pub(crate) fn new(protocol: ScopedProtocol<'a, Pointer<'a>>) -> Self {
        Self {
            protocol,
            x: 0.0,
            y: 0.0,
            left: false,
            right: false,
        }
    }

    pub(crate) fn get_event(&mut self) -> anyhow::Result<Option<MouseEvent>> {
        let Some(state) = self.protocol
            .read_state()
            .map_err(|e| anyhow!("Key read error: {:?}", e))?
            else { return Ok(None) };

        let (left, right) = state.button;

        let left_up = self.left && !left;
        let left_down = !self.left && left;
        let right_up = self.right && !right;
        let right_down = !self.right && right;

        self.left = left;
        self.right = right;

        let (dx, dy, _) = state.relative_movement;
        self.x += dx as f32;
        self.y += dy as f32;

        Ok(Some(MouseEvent {
            x: self.x,
            y: self.y,
            left_down,
            left_up,
            right_down,
            right_up,
        }))
    }
}
