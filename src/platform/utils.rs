use anyhow::anyhow;
use log::debug;
use uefi::{
    proto::Protocol,
    table::{boot::ScopedProtocol, Boot, SystemTable},
};

pub(crate) trait SystemExt {
    fn get_protocol<T: Protocol>(&self) -> anyhow::Result<ScopedProtocol<T>>;
}

impl SystemExt for SystemTable<Boot> {
    fn get_protocol<T: Protocol>(&self) -> anyhow::Result<ScopedProtocol<T>> {
        debug!("Getting protocol handle...");

        let handle = self
            .boot_services()
            .get_handle_for_protocol::<T>()
            .map_err(|e| anyhow!("Protocol get error: {:?}", e))?;

        debug!("Getting protocol instance...");

        let instance = self
            .boot_services()
            .open_protocol_exclusive::<T>(handle)
            .map_err(|e| anyhow!("Protocol open error: {:?}", e))?;

        Ok(instance)
    }
}
