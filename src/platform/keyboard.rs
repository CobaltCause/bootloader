use anyhow::anyhow;
use slint::platform::Key as SlintKey;
use uefi::{
    proto::console::text::{Input, Key as UefiKey, ScanCode},
    table::boot::ScopedProtocol,
};

pub(crate) struct KeyboardWrapper<'a> {
    protocol: ScopedProtocol<'a, Input>,
}

impl<'a> KeyboardWrapper<'a> {
    pub(crate) fn new(protocol: ScopedProtocol<'a, Input>) -> Self {
        Self { protocol }
    }

    pub(crate) fn get_event(&mut self) -> anyhow::Result<Option<char>> {
        let maybe_key = self
            .protocol
            .read_key()
            .map_err(|e| anyhow!("Key read error: {:?}", e))?;

        Ok(maybe_key.and_then(|k| match k {
            UefiKey::Printable(c) => Some(match c.into() {
                // windows-ass protocol
                '\r' => '\n',
                c => c,
            }),
            UefiKey::Special(sk) => match sk {
                ScanCode::UP => Some(SlintKey::UpArrow),
                ScanCode::DOWN => Some(SlintKey::DownArrow),
                ScanCode::LEFT => Some(SlintKey::LeftArrow),
                ScanCode::RIGHT => Some(SlintKey::RightArrow),
                _ => None,
            }
            .map(|x| x.into()),
        }))
    }
}
