use core::{
    cell::RefCell,
    sync::atomic::{AtomicBool, Ordering},
};

use alloc::{boxed::Box, rc::Rc, sync::Arc};
use chrono::{DateTime, Utc};
use log::{debug, warn};
use slint::{
    platform::{
        software_renderer::MinimalSoftwareWindow, EventLoopProxy, Platform, PointerEventButton,
        WindowEvent,
    },
    EventLoopError, LogicalPosition, PhysicalSize,
};
use uefi::{
    proto::console::{gop::GraphicsOutput, pointer::Pointer, text::Input},
    table::{Boot, SystemTable},
};

use self::{
    graphics::GraphicsWrapper, keyboard::KeyboardWrapper, mouse::MouseWrapper, time::TimeWrapper,
    utils::SystemExt,
};

pub(crate) mod graphics;
pub(crate) mod keyboard;
pub(crate) mod mouse;
pub(crate) mod time;
pub(crate) mod utils;

type UefiWindow = Rc<MinimalSoftwareWindow<1>>;

pub(crate) struct UefiPlatform<'a> {
    time: TimeWrapper<'a>,
    graphics: RefCell<GraphicsWrapper<'a>>,
    keyboard: RefCell<KeyboardWrapper<'a>>,
    mouse: RefCell<MouseWrapper<'a>>,

    start: DateTime<Utc>,
    // FIXME: consider double buffering
    window: UefiWindow,

    should_stop: Arc<AtomicBool>,
}

struct UefiEventLoopProxy {
    stop: Arc<AtomicBool>,
}

impl UefiEventLoopProxy {
    fn new(stop: Arc<AtomicBool>) -> Self {
        UefiEventLoopProxy { stop }
    }
}

impl EventLoopProxy for UefiEventLoopProxy {
    fn quit_event_loop(&self) -> Result<(), EventLoopError> {
        self.stop.store(true, Ordering::SeqCst);
        Ok(())
    }

    fn invoke_from_event_loop(
        &self,
        _event: Box<dyn FnOnce() + Send>,
    ) -> Result<(), EventLoopError> {
        panic!("Not supported!")
    }
}

impl<'a> UefiPlatform<'a> {
    pub(crate) fn new(
        window: UefiWindow,
        system_table: &SystemTable<Boot>,
    ) -> anyhow::Result<Self> {
        // SAFETY: We will not touch this after we've exited Slint, which needs to happen to exit boot services.
        let system_table = Box::leak(Box::new(unsafe { system_table.unsafe_clone() }));

        let protocol = system_table.get_protocol::<GraphicsOutput>()?;
        let graphics = GraphicsWrapper::new(protocol);
        let (width, height) = graphics.resolution();
        window.set_size(slint::PhysicalSize::new(width as u32, height as u32));

        let protocol = system_table.get_protocol::<Input>()?;
        let keyboard = KeyboardWrapper::new(protocol);

        let protocol = system_table.get_protocol::<Pointer>()?;
        let mouse = MouseWrapper::new(protocol);

        let runtime = system_table.runtime_services();
        let time = TimeWrapper::new(runtime);

        let now = time.get_time();

        Ok(Self {
            time,
            graphics: RefCell::new(graphics),
            keyboard: RefCell::new(keyboard),
            mouse: RefCell::new(mouse),
            start: now,
            window,
            should_stop: Arc::new(false.into()),
        })
    }
}

impl Platform for UefiPlatform<'_> {
    fn create_window_adapter(&self) -> Rc<dyn slint::platform::WindowAdapter> {
        self.window.clone()
    }

    fn duration_since_start(&self) -> core::time::Duration {
        (self.time.get_time() - self.start)
            .to_std()
            .expect("Time overflow!")
    }

    fn debug_log(&self, arguments: core::fmt::Arguments) {
        debug!("{}", arguments);
    }

    fn run_event_loop(&self) {
        let mut graphics = self.graphics.borrow_mut();
        let mut keyboard = self.keyboard.borrow_mut();
        let mut mouse = self.mouse.borrow_mut();

        let (width, height) = graphics.resolution();
        self.window.set_size(PhysicalSize {
            width: width as u32,
            height: height as u32,
        });

        while !self.should_stop.load(Ordering::SeqCst) {
            slint::platform::update_timers_and_animations();

            self.window.draw_if_needed(|renderer| {
                debug!("Repaint!");
                renderer.render(graphics.buffer_mut(), width);

                if let Err(e) = graphics.flip() {
                    warn!("Error when blitting: {:?}", e);
                }
            });

            if let Ok(Some(ch)) = keyboard.get_event() {
                debug!("Pressed: {:?}", ch);
                self.window
                    .dispatch_event(WindowEvent::KeyPressed { text: ch });
                self.window
                    .dispatch_event(WindowEvent::KeyReleased { text: ch });
            }

            if let Ok(Some(event)) = mouse.get_event() {
                debug!("Mouse event: {:?}", event);
                let position = LogicalPosition {
                    x: event.x,
                    y: event.y,
                };

                self.window
                    .dispatch_event(WindowEvent::PointerMoved { position });
                if event.left_up {
                    self.window.dispatch_event(WindowEvent::PointerReleased {
                        position,
                        button: PointerEventButton::Left,
                    });
                }
                if event.left_down {
                    self.window.dispatch_event(WindowEvent::PointerPressed {
                        position,
                        button: PointerEventButton::Left,
                    });
                }
                if event.right_up {
                    self.window.dispatch_event(WindowEvent::PointerReleased {
                        position,
                        button: PointerEventButton::Right,
                    });
                }
                if event.right_down {
                    self.window.dispatch_event(WindowEvent::PointerPressed {
                        position,
                        button: PointerEventButton::Right,
                    });
                }
            }
        }
    }

    fn new_event_loop_proxy(
        &self,
    ) -> Option<alloc::boxed::Box<dyn slint::platform::EventLoopProxy>> {
        Some(Box::new(UefiEventLoopProxy::new(self.should_stop.clone())))
    }
}
