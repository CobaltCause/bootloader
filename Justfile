run: build
    uefi-run --bios-path="${OVMF}/FV/OVMF.fd" --boot target/x86_64-unknown-uefi/debug/bootloader.efi -- -serial stdio

build:
    cargo build
