# Project Bootloader

Not actually bootloading yet. Please send help.

## Hacking

- Install [Nix](https://nixos.org)
- Enter a development shell with `nix develop`
- Run `just` to build and run the bootloader in a QEMU instance

## TODO list
- [ ] Make it actually boot things
- [ ] Make it pretty
- [ ] Wire up systemd-boot style config parsing
- [ ] Wire up all the secure boot stuff
- [ ] Test test test test test test test test test test test test test

## Maybe TODO list
- [ ] Some sort of persistent log store for devices with no serial (i.e. no QEMU)
- [ ] Dynamic theme loading
- [ ] More?